function HW2Q1GenerateData()
    m01 = [-1; -1];
    m02 = [1; 1];
    m11 = [-1; 2];
    m12 = [1; -1];
    C = [1, 0;
         0, 1];
     
    mu = [m01, m02, m11, m12];
     
    
    dataset = {};
    dataset.prior = [0.6, 0.4]; % Label 0 and 1 respectively
    dataset.dimensions = 2;
    dataset.class0Weights = [0.5, 0.5];
    dataset.class1Weights = [0.5, 0.5];
    dataset.mu = mu;
    dataset.C = C;
    rng('default')  % For reproducibility
    
    % WE USE CLASS PRIORS/WEIGHTS IN GMM TO PICK UP SAMPLES SO THAT THE
    % DISTRIBUTION IS CORRECT
    num_samples = 20;
    dataset_name = 'Dtrain20.mat'
    dataset = randGMM(dataset, mu, C, num_samples);
    y = dataset.x;

    figure(1);
    scatter(y(dataset.label == 0, 1), y(dataset.label ==0, 2), 'red');
    hold on;
    scatter(y(dataset.label == 1, 1), y(dataset.label ==1, 2), 'green');
    legend('class 0', 'class 1');
    xlabel('x1');
    ylabel('x2');
    title('Data distribution of x')
    
    save(dataset_name, 'dataset')

    function dataset = randGMM(dataset, mu, C, num_samples)
        dataset.num_samples = num_samples;
        dataset.label = rand(num_samples, 1) >= dataset.prior(1);
        dataset.samples_per_label = [sum(dataset.label == 0), sum(dataset.label == 1)];
        x = zeros(num_samples, dataset.dimensions);
        for i = 1:num_samples
            if dataset.label(i) == 0
                if rand(1,1) > dataset.class0Weights
                    x(i, :) = mvnrnd(mu(:, 1), C, 1);
                else
                    x(i, :) = mvnrnd(mu(:, 2), C, 1);
                end
            else 
                if rand(1,1) > dataset.class1Weights
                    x(i, :) = mvnrnd(mu(:, 3), C, 1);
                else
                    x(i, :) = mvnrnd(mu(:, 4), C, 1);
                end
            end
        end
        dataset.x = x;
    end
end