clc
clear all
close all
ds_train20 = load('Dtrain20', 'dataset').dataset;
ds_train200 = load('Dtrain200', 'dataset').dataset;
ds_train2000 = load('Dtrain2000', 'dataset').dataset;
ds_validate = load('Dvalidate10K', 'dataset').dataset;

theta20 = classify(ds_train20, 'training set');
classify(ds_validate, 'validation set, trained on 20 sample training data', theta20);

theta200 = classify(ds_train200, 'training set');
classify(ds_validate, 'validation set, trained on 200 sample training data', theta200);

theta2000 = classify(ds_train2000, 'training set');
classify(ds_validate, 'validation set, trained on 2000 sample training data', theta2000);
% b_x_validate = [ones(1, num_samples); x'];

function params = classify(ds, plot_title, theta)
    x = ds.x;
    label = ds.label;
    dimensions = ds.dimensions;
    num_samples = ds.num_samples;
    x_square = x'.*x';
    x1_square = x_square(1,:);
    x1x2 = (x(:,1).*x(:,2))';
    x2_square = x_square(2,:);
    b_x = [ones(1, num_samples); x'; x1_square; x1x2; x2_square];
    
    
    if ~exist('theta','var')
        % theta parameter does not exist, so it means this dataset
        % is for training
        initial_estimate = zeros(size(b_x, 1), 1);
        [theta, cost] = fminsearch(@(t)(cost_func(t, num_samples, label, b_x)), initial_estimate);
        params = theta;
    end
    
    p_error = plot_data(x, label, b_x, theta, plot_title);
    fprintf('Probability of error for %u sample %s = %f \n', num_samples, plot_title, p_error);

end

function p_error = plot_data(x, label, b_x, theta, plot_title)

    decision = b_x' * theta >= 0;
    cc00 = label == 0 & decision == 0;
    cc01 = label == 0 & decision == 1;
    cc10 = label == 1 & decision == 0;
    cc11 = label == 1 & decision == 1;

    figure;
    scatter(x(cc00, 1), x(cc00, 2), 'o', 'green');
    hold on;
    scatter(x(cc01, 1), x(cc01, 2), 'o', 'red');
    hold on;
    scatter(x(cc11, 1), x(cc11, 2), '^', 'green');
    hold on;
    scatter(x(cc10, 1), x(cc10, 2), '^', 'red');
    hold on
    
    % calculation for plotting decision boundary
    h_grid = linspace(floor(min(x(:, 1))) - 1, ceil(max(x(:, 1))) + 1);
    v_grid = linspace(floor(min(x(:, 2))) - 1, ceil(max(x(:, 2))) + 1);
    [h, v] = meshgrid(h_grid, v_grid);
    h = reshape(h, length(h_grid)*length(v_grid), 1);
    v = reshape(v, length(h_grid)*length(v_grid), 1);
    
    x_grid = [h,v];
    x_gridsquare = x_grid'.*x_grid';
    x1_gridsquare = x_gridsquare(1,:);
    x1x2_grid = (x_grid(:,1).*x_grid(:,2))';
    x2_gridsquare = x_gridsquare(2,:);
    b_x_grid = [ones(1, length(h)); x_grid'; x1_gridsquare; x1x2_grid; x2_gridsquare];
    decision_grid = b_x_grid' * theta;
    
    
    % Decision boundary exists at decision_grid = 0
    decision_grid = reshape(decision_grid, length(h_grid), length(v_grid));

    contour(h_grid, v_grid, decision_grid, [0, 0], 'b');
    hold off;
    legend('class 0 correct', 'class 0 incorrect', 'class 1 correct', 'class 1 incorrect', 'Decision Boundary', 'FontSize', 12);
    
    hold off;
    legend('class 0 correct', 'class 0 incorrect', 'class 1 correct', 'class 1 incorrect', 'Decision Boundary', 'FontSize', 12);
    xlabel('x1', 'FontSize', 12);
    ylabel('x2', 'FontSize', 12);

    title([num2str(size(x, 1)), ' sample data distribution of x with decision labels for ', plot_title], 'FontSize', 12)
    
    p_error = (sum(cc01) + sum(cc10))/size(x, 1);
end

function cost = cost_func(w, num_samples, label, x)
    h = 1./(1 + exp(-1*(w'*x)));
    cost = (-1/num_samples)*sum(label'.* log(h) + (1 - label').* log(1 - h));
end