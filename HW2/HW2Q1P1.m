clc
clear all
close all
ds = load('Dvalidate10K', 'dataset').dataset;

% ROC CURVE: Minimum probability of Error classifier

class_prior = ds.prior;
num_samples = ds.num_samples;
num_dimensions = ds.dimensions;
label = ds.label;
num_samples_per_label = ds.samples_per_label;
x = ds.x;

% pdf output given the samples
y = log((ds.class1Weights(1)*mvnpdf(ds.x, ds.mu(:, 3)', ds.C) + ds.class1Weights(1)*mvnpdf(ds.x, ds.mu(:, 4)', ds.C))./...
    (ds.class0Weights(1)*mvnpdf(ds.x, ds.mu(:, 1)', ds.C) + ds.class0Weights(1)*mvnpdf(ds.x, ds.mu(:, 2)', ds.C)));

% Add small value to prevent equality conditions while doing ERM likelihood ratio test
eps = 10e-6;

% Thresholds at mid points of discriminant scores (y)
sort_y = sort(y);
log_thresholds = [sort_y(1) - eps; (sort_y(1:end-1) + sort_y(2:end))/2; sort_y(end) + eps]';

[min_error, min_index, TPR, FPR] = calculateROC(log_thresholds, y, ds);
fprintf("Min-P(error) achievable on Dvalidate10K: %f", min_error);
min_decision = y >= log_thresholds(min_index);

gamma = 10^log_thresholds(min_index);

gamma_theoretical = ds.prior(1)/ds.prior(2);
TP_ideal = 0;
FP_ideal = 0;
for j = 1: num_samples
    if y(j) >= log(gamma_theoretical)
        % Implies decision is label 1
        % Now compare with the actual label from the data
        if label(j) == 1
            TP_ideal = TP_ideal + 1;
        else
            FP_ideal = FP_ideal + 1;
        end    
    end
end
TPR_ideal = TP_ideal/num_samples_per_label(2);
FPR_ideal = FP_ideal/num_samples_per_label(1);
min_error_ideal = FPR_ideal * class_prior(1) + (1 - TPR_ideal) * class_prior(2); % FNR = 1 - TPR

figure
plot(FPR, TPR, '-', FPR(min_index), TPR(min_index), 'o', FPR_ideal, TPR_ideal, '+')
legend('ROC Curve', 'Calculated minimum error', 'Theoretical minimum error', 'FontSize', 12)
xlabel('False Positive Rate', 'FontSize', 12)
ylabel('True Positive Rate', 'FontSize', 12)
title("Minimum P-ERROR classifier for 10K validation samples", 'FontSize', 12)

cc00 = ds.label == 0 & min_decision == 0;
cc01 = ds.label == 0 & min_decision == 1;
cc10 = ds.label == 1 & min_decision == 0;
cc11 = ds.label == 1 & min_decision == 1;

figure;
scatter(x(cc00, 1), x(cc00, 2), 'o', 'green');
hold on;
scatter(x(cc01, 1), x(cc01, 2), 'o', 'red');
hold on;
scatter(x(cc11, 1), x(cc11, 2), '^', 'green');
hold on;
scatter(x(cc10, 1), x(cc10, 2), '^', 'red');
hold on

% SUPPLEMENTARY VISUALIZATION: Calculation for plotting decision boundary
h_grid = linspace(floor(min(x(:, 1))) - 1, ceil(max(x(:, 1))) + 1);
v_grid = linspace(floor(min(x(:, 2))) - 1, ceil(max(x(:, 2))) + 1);
[h, v] = meshgrid(h_grid, v_grid);
h = reshape(h, length(h_grid)*length(v_grid), 1);
v = reshape(v, length(h_grid)*length(v_grid), 1);
y_score_grid = log((ds.class1Weights(1)*mvnpdf([h,v], ds.mu(:, 3)', ds.C) + ds.class1Weights(1)*mvnpdf([h,v], ds.mu(:, 4)', ds.C))./...
    (ds.class0Weights(1)*mvnpdf([h,v], ds.mu(:, 1)', ds.C) + ds.class0Weights(1)*mvnpdf([h,v], ds.mu(:, 2)', ds.C)));
y_score_grid = y_score_grid - log_thresholds(min_index); % Decision boundary exists at y_score_grid = 0
y_score_grid = reshape(y_score_grid, length(h_grid), length(v_grid));

contour(h_grid, v_grid, y_score_grid, [0, 0], 'b');
hold off;
legend('class 0 correct', 'class 0 incorrect', 'class 1 correct', 'class 1 incorrect', 'Decision Boundary', 'FontSize', 12);
xlabel('x1', 'FontSize', 12);
ylabel('x2', 'FontSize', 12);

title('10000 sample data distribution of x with decision boundary', 'FontSize', 12)
