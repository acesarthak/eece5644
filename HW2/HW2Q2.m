clc
clear all
close all
rng('default')  % For reproducibility
[xTrain,yTrain,xValidate,yValidate] = HW2Q2GenerateData(100, 1000);

% ML estimator
w = estimate(xTrain, yTrain);
[yCalculated, average_error_ML] = validationError(xValidate, yValidate, w);

figure, plot3(xValidate(1,:),xValidate(2,:),yCalculated,'.'), axis equal,
xlabel('x1', 'FontSize', 12),ylabel('x2', 'FontSize', 12), zlabel('y', 'FontSize', 12), title('Dataset created using trained parameters(ML) with no added noise(v)', 'FontSize', 12)

% MAP estimator
m = 7;
n = 6;
gamma_range = logspace(-m, n, 10^4);

[gamma, yCalculated, average_error_MAP] = bestMAPEstimate(gamma_range,xTrain,yTrain,xValidate,yValidate);

figure, plot3(xValidate(1,:),xValidate(2,:),yCalculated,'.'), axis equal,
xlabel('x1', 'FontSize', 12),ylabel('x2', 'FontSize', 12), zlabel('y', 'FontSize', 12), title('Dataset created using trained parameters(MAP) with no added noise(v)', 'FontSize', 12)

fprintf("Average squared error on validation set(ML estimate) = %f\n", average_error_ML);
fprintf("Average squared error on validation set(MAP estimate) = %f\n", average_error_MAP);

function [best_gamma, best_yCalculated, min_average_error] = bestMAPEstimate(gamma_range,xTrain,yTrain,xValidate,yValidate)
    min_average_error = 10^10;
    average_error_range = zeros(1, length(gamma_range));
    for i = 1:length(gamma_range)
        gamma = gamma_range(i);
        w = estimate(xTrain, yTrain, gamma);
        [yCalculated, average_error] = validationError(xValidate, yValidate, w);
        average_error_range(i) = average_error;
        if average_error < min_average_error
            best_gamma = gamma;
            best_yCalculated = yCalculated;
            min_average_error = average_error;
        end
    end
    % Plot average error with hyperparameter gamma
    figure, loglog(gamma_range, average_error_range)
    xlabel('Gamma (\gamma)', 'FontSize', 12),ylabel('Average Squared Error', 'FontSize', 12), title('loglog graph for MAP performance with \gamma', 'FontSize', 12)

end
    

function [yCalculated, average_error] = validationError(x, y, w)
    % calculate yCalculated on validation dataset
    val_samples = size(x, 2);
    yCalculated = zeros(1, val_samples);
    average_error = 0;
    for i = 1:val_samples
        b_x_ = [1; x(:, i); x(:, i).^2; x(:, i).^3];
        y_calculated = b_x_'*w;
        yCalculated(i) = y_calculated;
        average_error = average_error + ((y_calculated - y(i))^2)/val_samples;
    end
end

function params = estimate(x, y, gamma)
    [dim, samples] = size(x);
    R = zeros(7, 7);
    q = zeros(7, 1);
    for i = 1: samples
        b_x = [1; x(:, i); x(:, i).^2; x(:, i).^3];
        R = R + (b_x*b_x')./samples;
        q = q + (y(i)*b_x)./samples;
    end
    if exist('gamma','var')
        R = R + (1/(gamma*samples))*eye(7,7);
    end
    params = linsolve(R, q);
end