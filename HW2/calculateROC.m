function [min_error, min_index, TPR, FPR] = calculateROC(thresholds, y, dataset)
    class_prior = dataset.prior;
    num_samples = dataset.num_samples;
    label = dataset.label;
    num_samples_per_label = dataset.samples_per_label;
    x = dataset.x;
    num_thresholds = size(thresholds);
    TP = zeros(num_thresholds);
    FP = zeros(num_thresholds);

    for i = 1: size(thresholds, 2)
        for j = 1: num_samples
            if y(j) >= thresholds(i)
                % Implies decision is label 1
                % Now compare with the actual label from the data
                if label(j) == 1
                    TP(i) = TP(i) + 1;
                else
                    FP(i) = FP(i) + 1;
                end    
            end
        end
    end

    % ROC curve is a curve with FPR on x axis and TPR on y axis

    TPR = TP/num_samples_per_label(2);
    FPR = FP/num_samples_per_label(1);
    prob_error = FPR * class_prior(1) + (1 - TPR) * class_prior(2); % FNR = 1 - TPR
    [min_error, min_index] = min(prob_error);
    
end