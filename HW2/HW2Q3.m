clc
clear all
close all

radius = rand(1);
angle = rand(1)*2*pi;
true_position = [radius*cos(angle); radius*sin(angle)];
sigma = 0.3;
sigma_x = 0.25;
sigma_y = 0.25;

K = linspace(1,101, 101);
deviation = zeros(1, length(K)); 
for i = 1: length(K)
    num_landmarks = K(i);
    landmarks = zeros(2, num_landmarks);
    range_measurements = zeros(num_landmarks, 1);
    true_distances = zeros(num_landmarks, 1);
    for j = 1: num_landmarks
        angle = (2*pi*j)/num_landmarks;
        landmarks(:, j) = [cos(angle); sin(angle)];
        dj = norm(true_position - landmarks(:, j)); 
        while true
            nj = normrnd(0, sigma);
            if (dj + nj) > 0
                true_distances(j) = dj;
                range_measurements(j) = dj +nj;
                break;
            end
        end           
    end

    objective = @(x,y) ((x/sigma_x).^2 + (y/sigma_y).^2)./num_landmarks;

    for j = 1: num_landmarks
        d = @(x,y) sqrt((x - landmarks(1, j)).^2 + (y - landmarks(2, j)).^2);
        f = @(x,y) ((1./(sigma.^2)).*(range_measurements(j) - d(x,y)).^2)./num_landmarks;
        objective = @(x,y) objective(x,y) + f(x,y);
    end
    
    obj = @(x) objective(x(1),x(2)); % Because fminsearch accepts 1 input in vectorized form
    MAP_estimate = fminsearch(obj, [0,0]);
    deviation(i) = norm(true_position - MAP_estimate');
    
    if num_landmarks <= 4
        figure
        h = fcontour(objective, [-2,2], 'LevelList', 0:2:100);
        hold on;
        
        scatter(landmarks(1, :), landmarks(2, :), 'o', 'k');
        for j = 1: num_landmarks
            plotObjValue(landmarks(1, j), landmarks(2, j), objective)
        end
        
        scatter(true_position(1), true_position(2), '+', 'r');
        plotObjValue(true_position(1), true_position(2), objective)

        scatter(MAP_estimate(1), MAP_estimate(2), '^', 'r');

        xlabel('x axis', 'FontSize', 12),ylabel('y axis', 'FontSize', 12);
        title(['MAP estimation objective equilevel contours, K = ', num2str(num_landmarks)], 'FontSize', 12);
        legend('contours', 'landmarks with objective function values', 'true position with objective function value', 'minima of objective function/MAP estimate', 'FontSize', 12);

    end    
end
figure
plot(K, deviation)
xlabel('Number of landmarks (K)', 'FontSize', 12), ylabel('Distance between true position and MAP estimate', 'FontSize', 12);
title('Variation of MAP estimates with K', 'FontSize', 12);

function plotObjValue(x, y, obj_fn)
    objective_val = obj_fn(x,y);
    textString = sprintf('(%d)', objective_val);
    text(x-0.1, y-0.1, textString, 'FontSize', 12);
end
    

