function mlpKfold(DTrain, DTest, K)
    M = 25; % Max fifteen perceptrons
    pErrorVal = ones(K, M);

    % Generate Training Data
    x = DTrain.x';
    label = DTrain.label';

    % Divide the data set into K approximately-equal-sized partitions
    N = size(x, 2);
    dummy = ceil(linspace(0,N,K+1));
    for k = 1:K
        indPartitionLimits(k,:) = [dummy(k)+1,dummy(k+1)];
    end

    % Model Order Selection
    % Try several numbers-of-perceptrons   
    for m = 1:M
        % K-fold cross validation
        for k = 1:K
            indValidate = [indPartitionLimits(k,1):indPartitionLimits(k,2)];
            xValidate = x(:, indValidate); % Using folk k as validation set
            lValidate = label(:, indValidate);
            if k == 1
                inDTrain = [indPartitionLimits(k+1,1):N];
            elseif k == K
                inDTrain = [1:indPartitionLimits(k-1,2)];
            else
                inDTrain = [1:indPartitionLimits(k-1,2),indPartitionLimits(k+1,1):N];
            end
            xTrain = x(:, inDTrain); % using all other folds as training set
            lTrain = label(:, inDTrain);

            % Train model parameters
            [pErrorVal(k,m), ~] = nnModel(m, xTrain, lTrain, xValidate, lValidate);
            
        end
    end
    plot_objective_fn(mean(pErrorVal, 1))
    [pErrorBestM,bestM] = min(mean(pErrorVal, 1));
    % Train model parameters
    [~, net] = nnModel(bestM, x, label, x, label);
    % The decision which leads to max probability is chosen(after softmax)
    figure
    subplot(1, 2, 1)
    plotclassifier_output(net, x, label, "Training Data");
    subplot(1, 2, 2)
    plotclassifier_output(net, DTest.x', DTest.label', "Testing Data");
    fprintf("Optimal number of perceptrons = %f for MLP training.\n", bestM);

end

function [minPError, net] = nnModel(nPerceptrons, xTrain, lTrain, xValidate, lValidate)
    net = patternnet(nPerceptrons);
    net.layers{1}.transferFcn = 'poslin';
    out = zeros(2, size(xTrain, 2));
    for ind=1:2
        if ind == 1
            out(ind,:) = (lTrain==-1);
        elseif ind == 2
            out(ind,:) = (lTrain==1);
        end
    end
    net = train(net, xTrain, out);
    h = net(xValidate);
    
    [~,decision] = max(h,[],1);
    decision = 2*(decision-1.5);
    minPError = 1-length(find(decision==lValidate))/size(xValidate, 2);
end

function plot_objective_fn(pErrors)
    figure
    plot(linspace(1, size(pErrors,2), size(pErrors,2)),pErrors)
    xlabel('Number of Perceptrons(M)'), ylabel('Probability Of Error')
    title("Objective Function Model")
end
    
    
function plotclassifier_output(net, x, label, type)
    N = size(x, 2);
    h = net(x);
    [~,d] = max(h,[],1); % Labels of data using the trained MLP
    d = 2*(d-1.5);
    
    indINCORRECT = find(label.*d == -1); % Find samples that are incorrectly classified by the trained SVM
    indCORRECT = find(label.*d == 1); % Find samples that are correctly classified by the trained SVM
    pError = length(indINCORRECT)/N; % Empirical estimate of error probability

    plot(x(1,indCORRECT),x(2,indCORRECT),'g.'), hold on,
    plot(x(1,indINCORRECT),x(2,indINCORRECT),'r.'), axis equal, hold on
    title([type + " with pError = " + num2str(pError)])
    
    Nx = 1001; Ny = 1001; 
    xGrid = linspace(-10,10,Nx); 
    yGrid = linspace(-10,10,Ny);
    [q,v] = meshgrid(xGrid,yGrid); 
    hGrid = net([q(:),v(:)]');
    [~, dGrid] = max(hGrid,[],1); 
    dGrid = 2*(dGrid-1.5);
    zGrid = reshape(dGrid,Ny,Nx);
    colormap(gray)
    contour(q,v,zGrid,1); xlabel('x1'), ylabel('x2'), axis equal,
    legend("Correct", "Incorrect", "Decision Boundary")
end