clc
close all
clear all

image = imread("86000.jpg");
[R, C, D] = size(image);
% Generate Training Data
% x = HW4Q2GenerateData(image);

N = R * C; 
img = double(image);
rowIndices = (1 : R)' * ones(1, C); 
colIndices = ones(R, 1) * (1 : C);
features = [rowIndices(:)'; colIndices( : )']; % initialize with row and column indices

for d = 1 : D
    color = img(:, :, d); % pick one color at a time
    features = [features; color( : )'];
end

minf = min(features, [], 2); 
maxf = max(features, [], 2);
ranges = maxf - minf;
x = diag(ranges .^ (-1)) * (features - repmat(minf, 1, N)); % each feature normalized to the unit interval [0,1]


N = size(x, 2);
K = 10; % for K-fold c.v.
M = 10;

bestModel = cross_validate(x, length(x));
GMModel = fitgmdist(x', bestModel, 'RegularizationValue', 1e-10);
alpha = GMModel.ComponentProportion; %calculated alhpha
mu = (GMModel.mu)'; %calculated mu
sigma = GMModel.Sigma; %calculated sigma

for i = 1 : bestModel
    clusterpdf(i, :) = alpha(i) * evalGaussian(x, mu(:, i), sigma(:, :, i));
end

%MAP
[~, labels] = max(clusterpdf, [], 1);

labelImage = reshape(labels, R, C);
figure
subplot(1, 2, 1)
imshow(image)
subplot(1, 2, 2)
imshow(uint8(labelImage * 255 / bestModel));
title(strcat({'Best GMM order = '}, num2str(bestModel)));

% Model bestModel Selection
% Try several numbers-of-model bestModel(gmm components)   


function best_GMM = cross_validate(X, N)
    % bestModel-select using cross-validation
    % Performs EM algorithm to estimate parameters and evaluete performance
    % on each data set B times, with 1 through M GMM models considered
    K = 10; 
    M = 10;

    perfarray = zeros(K, M); %save space
    dummy = ceil(linspace(0, N, K+1));

    for k = 1 : K
        indPartitionLimits(k, :) = [dummy(k) + 1, dummy(k + 1)];
    end

    for k = 1 : K
        indValidate = (indPartitionLimits(k, 1) : indPartitionLimits(k, 2));

        if k == 1
            indTrain = (indPartitionLimits(k, 2) + 1 : N);
        elseif k == K
            indTrain = (1 : indPartitionLimits(k, 1) - 1);
        else
            indTrain = [1 : indPartitionLimits(k - 1, 2) indPartitionLimits(k + 1, 1) : N];
        end

        dValidate= X(:, indValidate); % Using folk k as validation set
        dTrain = X(:, indTrain); % using all other folds as training set
        NTrain = length(indTrain); 

        % Select GMM parameters for each bestModel
        for m = 1 : M
            GMModel = fitgmdist(dTrain',m,'RegularizationValue',1e-10);
            alpha = GMModel.ComponentProportion;%calculated alhpha
            mu = (GMModel.mu)';%calculated mu
            sigma = GMModel.Sigma;%calculated sigma
            %log-likelihood performence

            % Calculate log?likelihood performance with new parameters
            perf_array(k, m) = sum(log(evalGMM(dValidate, alpha, mu, sigma)));
            %keyboard;
        end
    end

    % Calculate average performance for each M and find best
    avg_perf = sum(perf_array) / K;
    [~, best_GMM] = max(avg_perf);

end

function gmm = evalGMM(x,alpha,mu,Sigma)
    gmm = zeros(1,size(x,2));
    for m = 1:length(alpha) % evaluate the GMM on the grid
        gmm = gmm + alpha(m)*evalGaussian(x,mu(:,m),Sigma(:,:,m));
    end
end 

function g = evalGaussian(x,mu,Sigma)
    [n,N] = size(x);
    invSigma = inv(Sigma);
    C = (2*pi)^(-n/2) * det(invSigma)^(1/2);
    E = -0.5*sum((x-repmat(mu,1,N)).*(invSigma*(x-repmat(mu,1,N))),1);
    g = C*exp(E);
end