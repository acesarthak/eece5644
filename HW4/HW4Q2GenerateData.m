function x = HW4Q2GenerateData(img)
%     imshow(img)
    img = double(img);
    R = img(:,:,1);
    G = img(:,:,2);
    B = img(:,:,3);
    R = (R - min(R,[],"all"))./(max(R,[],"all") -min(R,[],"all"));
    G = (G - min(G,[],"all"))./(max(G,[],"all") -min(G,[],"all"));
    B = (B - min(B,[],"all"))./(max(B,[],"all") -min(B,[],"all"));
    [rows,cols,~] = size(img);
    h = linspace(1, rows, rows);
    v = linspace(1, cols, cols);
    [H,V] = meshgrid(h,v);
    H = H';
    V = V';
    H = (H - min(H,[],"all"))./(max(H,[],"all") -min(H,[],"all"));
    V = (V - min(V,[],"all"))./(max(V,[],"all") -min(V,[],"all"));
    

    x = zeros(rows*cols, 5);
    for i = 1:rows
        for j = 1:cols
            x((i-1)*cols +j, :) = [H(i,j), V(i,j), R(i,j), G(i,j), B(i,j)];
        end
    end
    x = x';

end