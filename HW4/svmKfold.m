function svmKfold(DTrain, DTest, K)    
    % Train a Gaussian kernel SVM with cross-validation
    % to select hyperparameters that minimize probability 
    % of error (i.e. maximize accuracy; 0-1 loss scenario)

    x = DTrain.x';
    label = DTrain.label';
    
    C = optimizableVariable('C',[1e-5,1e9],'Transform','log');
    sigma = optimizableVariable('sigma',[1e-5,1e5],'Transform','log');
    params = {};
    params.C = C;
    params.sigma = sigma;
    f = @(params)kfoldLoss(params, x, label, K);
    results = bayesopt(f, [C, sigma], 'IsObjectiveDeterministic',true,...
    'PlotFcn',{@plotObjectiveModel,@plotMinObjective},'UseParallel',true,...
    'Verbose',0,'AcquisitionFunctionName','expected-improvement-plus');
    params = results.XAtMinObjective;
    CBest = params.C;
    sigmaBest = params.sigma;

    SVMBest = fitcsvm(x',label','BoxConstraint',CBest,'KernelFunction','gaussian','KernelScale',sigmaBest);
    figure
    subplot(1, 2, 1)
    plotclassifier_output(SVMBest, x, label, "Training Data");
    subplot(1, 2, 2)
    plotclassifier_output(SVMBest, DTest.x', DTest.label', "Testing Data")
    fprintf("Best C = %f and Best Sigma = %f for SVM training.\n", CBest, sigmaBest);
    
end

function [loss] = kfoldLoss(params, x, label, K)
    N = size(x, 2);
    dummy = ceil(linspace(0,N,K+1));
    for k = 1:K
        indPartitionLimits(k,:) = [dummy(k)+1,dummy(k+1)]; 
    end
    for k = 1:K
        indValidate = [indPartitionLimits(k,1):indPartitionLimits(k,2)];
        xValidate = x(:,indValidate); % Using folk k as validation set
        lValidate = label(:, indValidate);
    if k == 1
        indTrain = [indPartitionLimits(k,2)+1:N];
    elseif k == K
        indTrain = [1:indPartitionLimits(k,1)-1];
    else
        indTrain = [1:indPartitionLimits(k,1)-1,indPartitionLimits(k,2)+1:N];
    end
        % using all other folds as training set
        xTrain = x(:,indTrain); lTrain = label(indTrain);
        SVMk = fitcsvm(xTrain',lTrain,'BoxConstraint',params.C,'KernelFunction','gaussian','KernelScale',params.sigma);
        dValidate = SVMk.predict(xValidate')'; % Labels of validation data using the trained SVM
        indCORRECT = find(lValidate.*dValidate == 1); 
        Ncorrect(k)=length(indCORRECT);
    end
    loss = 1 - sum(Ncorrect)/N;
end

function plotclassifier_output(SVMBest, x, label, type)
    N = size(x, 2);
    d = SVMBest.predict(x')'; % Labels of data using the trained SVM
    indINCORRECT = find(label.*d == -1); % Find samples that are incorrectly classified by the trained SVM
    indCORRECT = find(label.*d == 1); % Find samples that are correctly classified by the trained SVM
    pError = length(indINCORRECT)/N; % Empirical estimate of error probability

    plot(x(1,indCORRECT),x(2,indCORRECT),'g.'), hold on,
    plot(x(1,indINCORRECT),x(2,indINCORRECT),'r.'), axis equal, hold on
    title([type + " with pError = " + num2str(pError)])
    
    Nx = 1001; Ny = 1001; 
    xGrid = linspace(-10,10,Nx); 
    yGrid = linspace(-10,10,Ny);
    [h,v] = meshgrid(xGrid,yGrid); 
    dGrid = SVMBest.predict([h(:),v(:)]); 
    zGrid = reshape(dGrid,Ny,Nx);
    colormap(gray)
    contour(h,v,zGrid,1); xlabel('x1'), ylabel('x2'), axis equal,
    legend("Correct", "Incorrect", "Decision Boundary")
end