clc
clear all;
close all;

% Prior probabilities are set to be equal
priors = [0.5, 0.5];
DTrain = HW4Q1GenerateData(1000, priors, "Training");
DTest = HW4Q1GenerateData(10000, priors, "Testing");
svmKfold(DTrain, DTest, 10);
mlpKfold(DTrain, DTest, 10);

