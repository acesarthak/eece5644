function [dataset] = HW4Q1GenerateData(num_samples, class_priors, type)

    r0 = 2; r1 = 4; sigma = 1;
    
    dataset = {};
    dataset.samples = num_samples;
    dataset.prior = class_priors;
    dataset.dimensions = 2;
    dataset.label = (rand(dataset.samples, 1) > class_priors(1)); % 0, 1 label
    dataset.label = 2*(dataset.label-0.5); % -1, 1 label
    dataset.samples_per_label = [sum(dataset.label == -1), sum(dataset.label == 1)];
    
    m = [0 0];
    C = [sigma.^2 0;
         0 sigma.^2];

    theta0 = 2*pi*rand(dataset.samples_per_label(1), 1);
    theta1 = 2*pi*rand(dataset.samples_per_label(2), 1);
    x = zeros(dataset.samples, dataset.dimensions);
    x(dataset.label == -1, :) = mvnrnd(m, C, dataset.samples_per_label(1)) + r0*[cos(theta0), sin(theta0)];
    x(dataset.label == 1, :) = mvnrnd(m, C, dataset.samples_per_label(2)) + r1*[cos(theta1), sin(theta1)];
    dataset.x = x;

    figure;
    plot(x(dataset.label == -1, 1), x(dataset.label == -1, 2), 'b.');
    hold on;
    plot(x(dataset.label == 1, 1), x(dataset.label == 1, 2), 'm.');
    hold on;
    legend('class -1', 'class 1');
    xlabel('x1');
    ylabel('x2');
    title([num2str(dataset.samples) + " " + type + ' sample data distribution of x'])

end