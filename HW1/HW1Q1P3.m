clc
clear all
load('dataset.mat', 'dataset')
mu1 = mean(dataset.x(dataset.label == 0, :));
sigma1 = cov(dataset.x(dataset.label == 0, :));
mu2 = mean(dataset.x(dataset.label == 1, :));
sigma2 = cov(dataset.x(dataset.label == 1, :));

Sb = (mu1 - mu2)'*(mu1 - mu2);
Sw = sigma1 + sigma2;

[V, D] = eig(Sb, Sw);
% Optimal wLDA is the generalized eigenvector of the matrix pair (Sb, Sw)
% which leads to laargest generalized eigen value.

[maxVal, maxInd] = max(max(D));

wLDA = V(:, maxInd);
yLDA = wLDA' * dataset.x';

figure
scatter(yLDA(dataset.label == 0), zeros(size(yLDA(dataset.label == 0))), 'red');
hold on
scatter(yLDA(dataset.label == 1), zeros(size(yLDA(dataset.label == 1))), 'green');
legend('class 0', 'class 1')
title("Fisher LDA Projection Vector")

% thresholds = linspace(min(yLDA), max(yLDA), 200);
% Add small value to prevent equality conditions
eps = 10e-6;

% Thresholds at mid points of discriminant scores (y)
sort_yLDA = sort(yLDA);
thresholds = [sort_yLDA(1) - eps, (sort_yLDA(1:end-1) + sort_yLDA(2:end))/2, sort_yLDA(end) + eps];
[min_error, min_index, TPR, FPR] = calculateROC(thresholds, yLDA, dataset);
tao_min_error = thresholds(min_index); 

figure
plot(FPR, TPR, '-', FPR(min_index), TPR(min_index), 'o')
legend('ROC Curve', 'Calculated minimum error')
xlabel('False Positive Rate')
ylabel('True Positive Rate')
title("Fisher LDA ROC Curve")