clc
clear all
% First run generateData.m to save dataset.mat file, then run this file if
% dataset.mat is not already present in the driectory

m0 =[-1/2 -1/2 -1/2 -1/2];
C0 =(1/4)*[2 -0.5 0.3 0;
           -0.5 1 -0.5 0;
           0.3 -0.5 1 0;
           0 0 0 2];

m1 =[1 1 1 1];
C1 =[1 0.3 -0.2 0;
     0.3 2 0.3 0;
     -0.2 0.3 1 0;
     0 0 0 3];

[min_error, gamma, min_error_ideal, gamma_theoretical] = helperHW1(m0, C0, m1, C1, "ERM Classifier ROC curve");

