clc
clear all
% First run generateData.m to save dataset.mat file, then run this file if
% dataset.mat is not already present in the driectory

m0_nb =[-1/2 -1/2 -1/2 -1/2];
C0_nb =(1/4)*[2 0 0 0;
              0 1 0 0;
              0 0 1 0;
              0 0 0 2];

m1_nb =[1 1 1 1];
C1_nb =[1 0 0 0;
        0 2 0 0;
        0 0 1 0;
        0 0 0 3];

[min_error_nb, gamma_nb, min_error_ideal_nb, gamma_theoretical_nb] = helperHW1(m0_nb, C0_nb, m1_nb, C1_nb, "Naive Bayes Classifier");