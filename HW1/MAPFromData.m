function [minError, conf_mat] = MAPFromData(x, x_train, label, label_train, num_samples, num_features, num_labels, alpha)
    unique(label);
    mus = zeros(num_labels, num_features);
    sigmas = zeros(num_features, num_features, num_labels);
    class_priors = zeros(1, num_labels);
    for i = 1: num_labels
        if ~isempty(x(label == i))
            mus(i, :) = mean(x(label == i, :));
            sigmas(:, :, i) = cov(x(label == i, :));
            lambda = alpha* trace(sigmas(:, :, i))/rank(sigmas(:, :, i));
            sigmas(:, :, i) = cov(x(label == i, :)) + lambda*eye(num_features, num_features);
            if isIllConditioned(decomposition(sigmas(:, :, i)))
                fprintf("Covariance matrix for label %d is ill conditioned \n", i);
            end
            class_priors(i) = size(x(label == i, :), 1)/num_samples;
        end
    end


    % PART A
    % MAP classifier (Given a sample, decide on the class label which leads to max posterior probability)

    [minError, conf_mat] = MAP(x_train, label_train, class_priors, mus, sigmas);

    [U, S, V] = svd(x, 'econ');
    figure
    pc1 = zeros(1, size(x, 1));
    pc2 = zeros(1, size(x, 1));
    pc3 = zeros(1, size(x, 1));

    for i = 1: size(x, 1)
        pc1(i) = V(:, 1)'*x(i, :)';
        pc2(i) = V(:, 2)'*x(i, :)';
        pc3(i) = V(:, 3)'*x(i, :)';
    end
    scatter3(pc1, pc2, pc3);
    xlabel('PCA1')
    ylabel('PCA2')
    zlabel('PCA3')
    title('Top three PCA for the data')

    figure
    subplot(1, 2, 1)
    semilogy(diag(S), 'k-o', 'LineWidth', 1)
    set(gca, 'FontSize', 15)
    axis tight
    grid on
    title("Eigen values");
    subplot(1, 2, 2)
    plot(cumsum(diag(S))./sum(diag(S)), 'k-o', 'LineWidth', 2.5)
    set(gca, 'FontSize', 15)
    axis tight
    grid on
    title("Percentage Contribution of each PCA/ Eigen value");

    function [minError, confusion_matrix] = MAP(x, label, priors, mus, sigmas)
        num_samples = size(x, 1);
        num_labels = size(priors, 2);
        p = zeros(num_labels, num_samples);
        for j = 1: num_labels
            if ~isempty(x(label == j))
                p(j, :) = (mvnpdf(x, mus(j, :), sigmas(:, :, j))*priors(j))';
            end
        end
        [maxVal, decision] = max(p);
        % Confusion Matrix: Decisions on rows, true labels on columns
        confusion_matrix = zeros(num_labels, num_labels);
        correct_classified = zeros(num_samples, 1);
        for i = 1: num_samples
            confusion_matrix(decision(i), label(i)) = confusion_matrix(decision(i), label(i)) + 1;
            if decision(i) == label(i)
                correct_classified(i) = 1;
            end       
        end
        minError = (num_samples - trace(confusion_matrix))/num_samples;
        % Confusion matrix using probabilities
        confusion_matrix = confusion_matrix./(sum(confusion_matrix));
        for i = 1: num_labels
            for j = 1: num_labels
                if isnan(confusion_matrix(i, j))
                    confusion_matrix(i, j) = 0;
                end
            end
        end
    end
end