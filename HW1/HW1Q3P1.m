clc;
close all;
clear all;

num_samples = 4898;
num_features = 11;
num_labels = 11;

% Read data from .csv file
x_and_label = readmatrix("dataset/winequality-white.csv");
x = x_and_label(:, 1:end-1);
label = x_and_label(:, end) + 1;

% Add 1 to all the labels so that labels start from 1 and match matlab
% indexing easily
[minError, conf_mat] = MAPFromData(x, x, label, label, num_samples, num_features, num_labels, 0.000009);
