function [min_error, gamma, min_error_ideal, gamma_theoretical] = helperHW1(m0, C0, m1, C1, plot_title)
    load('dataset.mat', 'dataset');
    class_prior = dataset.prior;
    num_samples = dataset.samples;
    num_dimensions = dataset.dimensions;
    label = dataset.label;
    num_samples_per_label = dataset.samples_per_label;
    x = dataset.x;

    % pdf output given the 10000 samples
    y = log(mvnpdf(x, m1, C1)./mvnpdf(x, m0, C0));
    
    % Add small value to prevent equality conditions while doing ERM likelihood ratio test
    eps = 10e-6;
    
    % Thresholds at mid points of discriminant scores (y)
    sort_y = sort(y);
    log_thresholds = [sort_y(1) - eps; (sort_y(1:end-1) + sort_y(2:end))/2; sort_y(end) + eps]';

    [min_error, min_index, TPR, FPR] = calculateROC(log_thresholds, y, dataset);

    gamma = 10^log_thresholds(min_index);

    gamma_theoretical = 7/13;
    TP_ideal = 0;
    FP_ideal = 0;
    for j = 1: num_samples
        if y(j) >= log(gamma_theoretical)
            % Implies decision is label 1
            % Now compare with the actual label from the data
            if label(j) == 1
                TP_ideal = TP_ideal + 1;
            else
                FP_ideal = FP_ideal + 1;
            end    
        end
    end
    TPR_ideal = TP_ideal/num_samples_per_label(2);
    FPR_ideal = FP_ideal/num_samples_per_label(1);
    min_error_ideal = FPR_ideal * class_prior(1) + (1 - TPR_ideal) * class_prior(2); % FNR = 1 - TPR
    
    figure
    plot(FPR, TPR, '-', FPR(min_index), TPR(min_index), 'o', FPR_ideal, TPR_ideal, '+')
    legend('ROC Curve', 'Calculated minimum error', 'Theoretical minimum error')
    xlabel('False Positive Rate')
    ylabel('True Positive Rate')
    title(plot_title)
end


