function generateData()
    m0 =[-1/2 -1/2 -1/2 -1/2];
    C0 =(1/4)*[2 -0.5 0.3 0;
               -0.5 1 -0.5 0;
               0.3 -0.5 1 0;
               0 0 0 2];

    m1 =[1 1 1 1];
    C1 =[1 0.3 -0.2 0;
         0.3 2 0.3 0;
         -0.2 0.3 1 0;
         0 0 0 3];
    dataset = {};
    dataset.prior = [0.35, 0.65]; % Label 0 and 1 respectively
    dataset.samples = 10000;
    dataset.dimensions = 4;
    rng('default')  % For reproducibility
    dataset.label = rand(dataset.samples, 1) >= dataset.prior(1);
    dataset.samples_per_label = [sum(dataset.label == 0), sum(dataset.label == 1)];


    x = zeros(dataset.samples, dataset.dimensions);
    x(dataset.label == 0, :) = mvnrnd(m0, C0, dataset.samples_per_label(1));
    x(dataset.label == 1, :) = mvnrnd(m1, C1, dataset.samples_per_label(2));
    dataset.x = x;

    size1 = x(dataset.label == 0, 4);
    size1 = size1 - min(size1) + 0.001;
    size2 = x(dataset.label == 1, 4);
    size2 = size2 - min(size2) + 0.001;

    figure(1);
    scatter3(x(dataset.label == 0, 1), x(dataset.label ==0, 2), x(dataset.label == 0, 3), size1, 'red');
    hold on;
    scatter3(x(dataset.label == 1, 1), x(dataset.label ==1, 2), x(dataset.label == 1, 3), size2, 'green');
    legend('class 0', 'class 1');
    xlabel('x1');
    ylabel('x2');
    zlabel('x3');

    title('10000 sample data distribution of x, 4th dimension represented by size of circles')
    save('dataset');
end