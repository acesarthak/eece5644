clc
clear all
close all

m0 = [0 0 0];
C0 = [1 0.5 0.5;
      0.5 1 0.5;
      0.5 0.5 1];

m1 = [0.25 0.25 0.25];
C1 = [4.5 -0.5 -0.5;
      -0.5 4.5 1.5;
      -0.5 1.5 4.5];
    
m2 = [-3.25 -3.25 -3.25];
C2 = [1.5 -0.5 -0.5;
      -0.5 1.0 1.5;
      -0.5 1.5 2.5];

m3 = [3.5 3.5 3.5];
C3 = [1 0.5 1;
      0.5 1.5 1.5;
      1 1.5 2];

dataset = {};
dataset.prior = [0.3, 0.3, 0.4]; % Label 1, 2 and 3 respectively
dataset.class3Weights = [0.5, 0.5];
dataset.samples = 10000;
dataset.dimensions = 3;
rng('default')  % For reproducibility
random_nums = rand(dataset.samples, 1); 

dataset.label = zeros(dataset.samples, 1);
for i = 1:dataset.samples
    if random_nums(i) < dataset.prior(1) % prior1 prob
        dataset.label(i) = 1;
    elseif random_nums(i) < dataset.prior(1) + dataset.prior(2) % prior2 prob
        dataset.label(i) = 2;
    else % prior 3 prob
        dataset.label(i) = 3; 
    end
end
        
dataset.samples_per_label = [sum(dataset.label == 1), sum(dataset.label == 2), sum(dataset.label == 3)];

% WE USE CLASS PRIORS/WEIGHTS IN GMM TO PICK UP SAMPLES SO THAT THE
% DISTRIBUTION IS CORRECT
x = zeros(dataset.samples, dataset.dimensions);
x(dataset.label == 1, :) = mvnrnd(m0, C0, dataset.samples_per_label(1));
x(dataset.label == 2, :) = mvnrnd(m1, C1, dataset.samples_per_label(2));
for i = 1:dataset.samples
    if dataset.label(i) == 3
        if rand(1,1) > dataset.class3Weights
            x(i, :) = mvnrnd(m3, C3, 1);
        else
            x(i, :) = mvnrnd(m2, C2, 1);
        end
    end
end
            
dataset.x = x;

figure;
scatter3(x(dataset.label == 1, 1), x(dataset.label == 1, 2), x(dataset.label == 1, 3), 'red');
hold on;
scatter3(x(dataset.label == 2, 1), x(dataset.label == 2, 2), x(dataset.label == 2, 3), 'green');
hold on;
scatter3(x(dataset.label == 3, 1), x(dataset.label == 3, 2), x(dataset.label == 3, 3), 'blue');
hold on;
legend('class 1', 'class 2', 'class 3');
xlabel('x1');
ylabel('x2');
zlabel('x3');

title('10000 sample data distribution of x')

% PART A
% MAP classifier (Given a sample, decide on the class label which leads to max posterior probability)

decision = zeros(dataset.samples, 1);
for i = 1: dataset.samples
    p = [0; 0; 0];
    p(1) = mvnpdf(x(i, :), m0, C0)*dataset.prior(1);
    p(2) = mvnpdf(x(i, :), m1, C1)*dataset.prior(2);
    p(3) = dataset.prior(3)*(dataset.class3Weights(1)*mvnpdf(x(i, :), m2, C2) + dataset.class3Weights(2)*mvnpdf(x(i, :), m3, C3));
    [min_risk01, maxInd] = max(p);
    decision(i) = maxInd;
end

min_risk01
conf_mat01 = plotDecisionWithGroundTruth(dataset, decision, "0-1 loss (MAP classifier)")
    
% PART B
loss_matrix10 = [0 1 10;
               1 0 10;
               1 1 0];
             
[decision, min_risk10] = ERM(dataset, m0, m1, m2, m3, C0, C1, C2, C3, loss_matrix10);
min_risk10
conf_mat10 = plotDecisionWithGroundTruth(dataset, decision, "L13, L23 = 10")

loss_matrix100 = [0 1 100;
               1 0 100;
               1 1 0];
             
[decision, min_risk100] = ERM(dataset, m0, m1, m2, m3, C0, C1, C2, C3, loss_matrix100);
min_risk100
conf_mat100 = plotDecisionWithGroundTruth(dataset, decision, "L13, L23 = 100")

function [decision, min_risk] = ERM(dataset, m0, m1, m2, m3, C0, C1, C2, C3, loss_matrix)
    decision = zeros(dataset.samples, 1);
    min_risk = 0;
    for i = 1: dataset.samples
        p = [0; 0; 0];
        p(1) = mvnpdf(dataset.x(i, :), m0, C0)*dataset.prior(1);
        p(2) = mvnpdf(dataset.x(i, :), m1, C1)*dataset.prior(2);
        p(3) = dataset.prior(3)*(dataset.class3Weights(1)*mvnpdf(dataset.x(i, :), m2, C2) + dataset.class3Weights(2)*mvnpdf(dataset.x(i, :), m3, C3));
        [risk, ind] = min(loss_matrix*p); % risk incurred for this decision
        min_risk = min_risk + risk;
        decision(i) = ind;
    end
    min_risk = min_risk/dataset.samples;
end

function [confusion_matrix] = plotDecisionWithGroundTruth(dataset, decision, plot_title)
    x = dataset.x;
    % Confusion Matrix: Decisions on rows, true labels on columns
    confusion_matrix = zeros(3, 3);
    correct_classified = zeros(dataset.samples, 1);
    for i = 1: dataset.samples
        confusion_matrix(decision(i), dataset.label(i)) = confusion_matrix(decision(i), dataset.label(i)) + 1;
        if decision(i) == dataset.label(i)
            correct_classified(i) = 1;
        end       
    end
    % Confusion matrix using probabilities
    confusion_matrix = confusion_matrix./(sum(confusion_matrix));

    cc11 = dataset.label == 1 & correct_classified == 1;
    cc10 = dataset.label == 1 & correct_classified == 0;
    cc21 = dataset.label == 2 & correct_classified == 1;
    cc20 = dataset.label == 2 & correct_classified == 0;
    cc31 = dataset.label == 3 & correct_classified == 1;
    cc30 = dataset.label == 3 & correct_classified == 0;
    figure;
    scatter3(x(cc11, 1), x(cc11, 2), x(cc11, 3), 'o', 'green');
    hold on;
    scatter3(x(cc10, 1), x(cc10, 2), x(cc10, 3), 'o', 'red');
    hold on;
    scatter3(x(cc21, 1), x(cc21, 2), x(cc21, 3), '^', 'green');
    hold on;
    scatter3(x(cc20, 1), x(cc20, 2), x(cc20, 3), '^', 'red');
    hold on;
    scatter3(x(cc31, 1), x(cc31, 2), x(cc31, 3), 'square', 'green');
    hold on;
    scatter3(x(cc30, 1), x(cc30, 2), x(cc30, 3), 'square', 'red');
    hold on;

    legend('class 1 correct', 'class 1 incorrect', 'class 2 correct', 'class 2 incorrect', 'class 3 correct', 'class 3 incorrect');
    xlabel('x1');
    ylabel('x2');
    zlabel('x3');

    title('10000 sample data distribution of x with decision labels with ' + plot_title)
end