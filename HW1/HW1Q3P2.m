clc;
close all;
clear all;

num_samples = 10299;
num_features = 561;
num_labels = 6;

% Read data from .txt file
formatSpec = '%f';

trainID = fopen('dataset/UCIHARDataset/UCIHARDataset/train/X_train.txt','r');
x_train = fscanf(trainID,formatSpec);
x_train = (reshape(x_train, num_features, []))'; 
trainIDLabel = fopen('dataset/UCIHARDataset/UCIHARDataset/train/y_train.txt','r');
label_train = fscanf(trainIDLabel,formatSpec);

testID = fopen('dataset/UCIHARDataset/UCIHARDataset/test/X_test.txt','r');
x_test = fscanf(testID,formatSpec);
x_test = (reshape(x_test, num_features, []))'; 
testIDLabel = fopen('dataset/UCIHARDataset/UCIHARDataset/test/y_test.txt','r');
label_test = fscanf(testIDLabel,formatSpec);

x = [x_train; x_test];
label = [label_train; label_test];

[minError, conf_mat] = MAPFromData(x, x_train, label, label_train, num_samples, num_features, num_labels, 0.196);
