function [alpha, mu, Sigma, logL] = EMforGMM(x, M, dp) % dim x num_samples, number of gaussian components
    % Uses EM algorithm to estimate the parameters of a GMM

    delta = 1e-2; % tolerance for EM stopping criterion
    regWeight = 1e-10; % regularization parameter for covariance estimates
    [d, N] = size(x); % determine dimensionality of samples and number of samples

    % Initialize the GMM to randomly selected samples
    alpha = ones(1,M)/M;
    shuffledIndices = randperm(N);
    mu = x(:,shuffledIndices(1:M)); % pick M random samples as initial mean estimates
    [~,assignedCentroidLabels] = min(pdist2(mu',x'),[],1); % assign each sample to the nearest mean
    for m = 1:M % use sample covariances of initial assignments as initial covariance estimates
        Sigma(:,:,m) = cov(x(:,find(assignedCentroidLabels==m))') + regWeight*eye(d,d);
    end
    t = 0;

    Converged = 0; % Not converged at the beginning
    while ~Converged || t < 200
        for l = 1:M
            temp(l,:) = repmat(alpha(l),1,N).*evalGaussian(x,mu(:,l),Sigma(:,:,l));
        end
        plgivenx = temp./sum(temp,1);
        alphaNew = mean(plgivenx,2);
        w = plgivenx./repmat(sum(plgivenx,2),1,N);
        muNew = x*w';
        for l = 1:M
            v = x-repmat(muNew(:,l),1,N);
            u = repmat(w(l,:),d,1).*v;
            SigmaNew(:,:,l) = u*v' + regWeight*eye(d,d); % adding a small regularization term
        end
        Dalpha = sum(abs(alphaNew-alpha'));
        Dmu = sum(sum(abs(muNew-mu)));
        DSigma = sum(sum(sum(abs(abs(SigmaNew-Sigma)))));
        Converged = ((Dalpha+Dmu+DSigma/2)<delta); % Check if converged
        alpha = alphaNew'; mu = muNew; Sigma = SigmaNew;
        t = t+1; 
        logLikelihood(t) = sum(log(evalGMM(x,alpha,mu,Sigma)))/N;
    end
    if dp
        displayProgress(x,alpha,mu,Sigma, logLikelihood);
    end
    logL = logLikelihood(end);
end

function displayProgress(x,alpha,mu,Sigma, logLikelihood)
    if size(x,1)==2
        subplot(1,3,2)
        plot(x(1,:),x(2,:),'g.'); 
        xlabel('x_1'), ylabel('x_2'), title('Data and Estimated GMM Contours'),
        axis equal, hold on;
        rangex1 = [min(x(1,:)),max(x(1,:))];
        rangex2 = [min(x(2,:)),max(x(2,:))];
        [x1Grid,x2Grid,zGMM] = contourGMM(alpha,mu,Sigma,rangex1,rangex2);
        contour(x1Grid,x2Grid,zGMM); axis equal, 
        subplot(1,3,3) 
    end
    plot(logLikelihood,'b.'); hold on,
    xlabel('Iteration Index'), ylabel('Log-Likelihood of Data per sample'),
    drawnow; 
end

function [x1Grid,x2Grid,zGMM] = contourGMM(alpha,mu,Sigma,rangex1,rangex2)
    x1Grid = linspace(floor(rangex1(1)),ceil(rangex1(2)),101);
    x2Grid = linspace(floor(rangex2(1)),ceil(rangex2(2)),91);
    [h,v] = meshgrid(x1Grid,x2Grid);
    GMM = (evalGMM([h(:)';v(:)'],alpha, mu, Sigma));
    zGMM = reshape(GMM,91,101);
end

function gmm = evalGMM(x,alpha,mu,Sigma)
    gmm = zeros(1,size(x,2));
    for m = 1:length(alpha) % evaluate the GMM on the grid
        gmm = gmm + alpha(m)*evalGaussian(x,mu(:,m),Sigma(:,:,m));
    end
end

function g = evalGaussian(x,mu,Sigma)
    [n,N] = size(x);
    invSigma = inv(Sigma);
    C = (2*pi)^(-n/2) * det(invSigma)^(1/2);
    E = -0.5*sum((x-repmat(mu,1,N)).*(invSigma*(x-repmat(mu,1,N))),1);
    g = C*exp(E);
end