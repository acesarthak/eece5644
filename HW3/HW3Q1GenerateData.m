function [dataset] = HW3Q1GenerateData(num_samples)
    m0 = [0 0 0];
    C0 = [2 0.5 0.5;
          0.5 2 0.5;
          0.5 0.5 2].*2;

    m1 = [-2.5 3.5 3.5];
    C1 = [2.5 1.5 -0.5;
          1.5 2.5 1.5;
          -0.5 1.5 2.5].*2;

    m2 = [-3.25 -3.25 -3.25];
    C2 = [3.5 -1.5 -0.5;
          -1.5 2.0 1.5;
          -0.5 1.5 2.5].*2;

    m3 = [3.5 3.5 3.5];
    C3 = [1 0.5 1;
          0.5 1.5 1.5;
          1 1.5 2].*2;

    dataset = {};
    dataset.prior = [0.25, 0.25, 0.25, 0.25]; % Label 1, 2, 3 and 4 respectively
    dataset.samples = num_samples;
    dataset.dimensions = 3;
    dataset.mean = [m0;m1;m2;m3];
    dataset.sigma = [C0;C1;C2;C3];
    rng('default')  % For reproducibility
    random_nums = rand(dataset.samples, 1); 

    dataset.label = zeros(dataset.samples, 1);
    for i = 1:dataset.samples
        if random_nums(i) < dataset.prior(1) % prior1 prob
            dataset.label(i) = 1;
        elseif random_nums(i) < dataset.prior(1) + dataset.prior(2) % prior2 prob
            dataset.label(i) = 2;
        elseif random_nums(i) < dataset.prior(1) + dataset.prior(2) + dataset.prior(3) % prior3 prob
            dataset.label(i) = 3;
        else % prior 4 prob
            dataset.label(i) = 4; 
        end
    end

    dataset.samples_per_label = [sum(dataset.label == 1), sum(dataset.label == 2), sum(dataset.label == 3), sum(dataset.label == 4)];

    % WE USE CLASS PRIORS/WEIGHTS IN GMM TO PICK UP SAMPLES SO THAT THE
    % DISTRIBUTION IS CORRECT
    x = zeros(dataset.samples, dataset.dimensions);
    x(dataset.label == 1, :) = mvnrnd(m0, C0, dataset.samples_per_label(1));
    x(dataset.label == 2, :) = mvnrnd(m1, C1, dataset.samples_per_label(2));
    x(dataset.label == 3, :) = mvnrnd(m2, C2, dataset.samples_per_label(3));
    x(dataset.label == 4, :) = mvnrnd(m3, C3, dataset.samples_per_label(4));
    
    y = zeros(dataset.samples, 1);
    y(dataset.label == 1, :) = mvnpdf(x(dataset.label == 1, :), m0, C0);
    y(dataset.label == 2, :) = mvnpdf(x(dataset.label == 2, :), m1, C1);
    y(dataset.label == 3, :) = mvnpdf(x(dataset.label == 3, :), m2, C2);
    y(dataset.label == 4, :) = mvnpdf(x(dataset.label == 4, :), m3, C3);

    dataset.x = x;
    dataset.y = y;

    figure;
    scatter3(x(dataset.label == 1, 1), x(dataset.label == 1, 2), x(dataset.label == 1, 3), 'red');
    hold on;
    scatter3(x(dataset.label == 2, 1), x(dataset.label == 2, 2), x(dataset.label == 2, 3), 'green');
    hold on;
    scatter3(x(dataset.label == 3, 1), x(dataset.label == 3, 2), x(dataset.label == 3, 3), 'blue');
    hold on;
    scatter3(x(dataset.label == 4, 1), x(dataset.label == 4, 2), x(dataset.label == 4, 3), 'yellow');
    hold on;
    legend('class 1', 'class 2', 'class 3', 'class 4');
    xlabel('x1');
    ylabel('x2');
    zlabel('x3');

    title([num2str(dataset.samples), ' sample data distribution of x'])
end