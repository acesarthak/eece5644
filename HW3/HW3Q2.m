clc
close all
clear all

num_iterations = 30;
samples = [10, 100, 1000, 10000];
num_cases = length(samples);
modelSelectionRate = zeros(6, num_cases);

for p = 1: num_iterations
    fprintf("For iteration number = %d /%d\n", p, num_iterations);
    bestModel = zeros(1, num_cases);
    for j = 1: num_cases

        % Generate Training Data
        N = samples(j);
        fprintf("Training samples = %d\n", N);
        dataset = HW3Q2GenerateData(N, true);
        x = dataset.x';
        K = 10; % for K-fold c.v.

        % Divide the data set into K approximately-equal-sized partitions
        dummy = ceil(linspace(0,N,K+1));
        for k = 1:K
            indPartitionLimits(k,:) = [dummy(k)+1,dummy(k+1)];
        end

        % Model Order Selection
        % Try several numbers-of-model order(gmm components)   
        for M = 1:6
            % K-fold cross validation
            for k = 1:K
                indValidate = [indPartitionLimits(k,1):indPartitionLimits(k,2)];
                xValidate = x(:, indValidate); % Using folk k as validation set
                if k == 1
                    indataset = [indPartitionLimits(k+1,1):N];
                elseif k == K
                    indataset = [1:indPartitionLimits(k-1,2)];
                else
                    indataset = [1:indPartitionLimits(k-1,2),indPartitionLimits(k+1,1):N];
                end
                xTrain = x(:, indataset); % using all other folds as training set
                Ntrain = length(indataset); Nvalidate = length(indValidate);

                [alpha, mu, Sigma, logLTrain] = EMforGMM(xTrain, M, false);
                logLValidate = sum(log(evalGMM(xValidate,alpha,mu,Sigma)));
                logL(k,M) = logLValidate/Nvalidate;

            end
        end
        [~,bestM] = max(mean(logL, 1));
        EMforGMM(x, bestM, true); % Change to true to see figures for each experiment
        bestModel(j) = bestM;
        modelSelectionRate(bestM, j) = modelSelectionRate(bestM, j) + 1;
    end
end

figure
bar(modelSelectionRate);
grid on;
title("Model Selection Rate Graph");
xlabel("Model Number(Number of Gaussian componenets)");
ylabel("Number of times this model was selected");
legend(["10 samples", "100 samples", "1000 samples", "10000 samples"]);

function gmm = evalGMM(x,alpha,mu,Sigma)
    gmm = zeros(1,size(x,2));
    for m = 1:length(alpha) % evaluate the GMM on the grid
        gmm = gmm + alpha(m)*evalGaussian(x,mu(:,m),Sigma(:,:,m));
    end
end 

function g = evalGaussian(x,mu,Sigma)
    [n,N] = size(x);
    invSigma = inv(Sigma);
    C = (2*pi)^(-n/2) * det(invSigma)^(1/2);
    E = -0.5*sum((x-repmat(mu,1,N)).*(invSigma*(x-repmat(mu,1,N))),1);
    g = C*exp(E);
end