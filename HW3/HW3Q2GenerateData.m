function [dataset] = HW3Q2GenerateData(num_samples, ifplot)
    m0 = [4 -5];
    C0 = [0.25 0;
          0 4;].*2;

    m1 = [-2.5 3.5];
    C1 = [2.5 1.5;
          1.5 2.5;].*2;

    m2 = [0 -2];
    C2 = [1.5 0;
          0 0.5;].*2;

    m3 = [3.5 3.5];
    C3 = [1 0.5;
          0.5 1.5;].*2;

    dataset = {};
    dataset.gmmWeights = [0.1, 0.2, 0.3, 0.4];
    dataset.samples = num_samples;
    dataset.dimensions = 2;
    dataset.mean = [m0;m1;m2;m3];
    dataset.sigma = [C0;C1;C2;C3];
%     rng('default')  % For reproducibility
    random_nums = rand(dataset.samples, 1); 

    dataset.label = zeros(dataset.samples, 1);
    for i = 1:dataset.samples
        if random_nums(i) < dataset.gmmWeights(1) % gmmWeights1 prob
            dataset.label(i) = 1;
        elseif random_nums(i) < dataset.gmmWeights(1) + dataset.gmmWeights(2) % gmmWeights2 prob
            dataset.label(i) = 2;
        elseif random_nums(i) < dataset.gmmWeights(1) + dataset.gmmWeights(2) + dataset.gmmWeights(3) % gmmWeights3 prob
            dataset.label(i) = 3;
        else % gmmWeights 4 prob
            dataset.label(i) = 4; 
        end
    end

    dataset.samples_per_label = [sum(dataset.label == 1), sum(dataset.label == 2), sum(dataset.label == 3), sum(dataset.label == 4)];

    % WE USE CLASS gmmWeights IN GMM TO PICK UP SAMPLES SO THAT THE
    % DISTRIBUTION IS CORRECT
    x = zeros(dataset.samples, dataset.dimensions);
    x(dataset.label == 1, :) = mvnrnd(m0, C0, dataset.samples_per_label(1));
    x(dataset.label == 2, :) = mvnrnd(m1, C1, dataset.samples_per_label(2));
    x(dataset.label == 3, :) = mvnrnd(m2, C2, dataset.samples_per_label(3));
    x(dataset.label == 4, :) = mvnrnd(m3, C3, dataset.samples_per_label(4));
    
    y = zeros(dataset.samples, 1);
    y(dataset.label == 1, :) = mvnpdf(x(dataset.label == 1, :), m0, C0);
    y(dataset.label == 2, :) = mvnpdf(x(dataset.label == 2, :), m1, C1);
    y(dataset.label == 3, :) = mvnpdf(x(dataset.label == 3, :), m2, C2);
    y(dataset.label == 4, :) = mvnpdf(x(dataset.label == 4, :), m3, C3);

    dataset.x = x;
    dataset.y = y;

    if ifplot
        figure;
        subplot(1,3,1)
        scatter(x(dataset.label == 1, 1), x(dataset.label == 1, 2), 'red');
        hold on;
        scatter(x(dataset.label == 2, 1), x(dataset.label == 2, 2), 'green');
        hold on;
        scatter(x(dataset.label == 3, 1), x(dataset.label == 3, 2), 'blue');
        hold on;
        scatter(x(dataset.label == 4, 1), x(dataset.label == 4, 2), 'yellow');
        hold on;
        legend('class 1', 'class 2', 'class 3', 'class 4');
        xlabel('x1');
        ylabel('x2');
        zlabel('x3');

        title([num2str(dataset.samples), ' sample data distribution of x'])
    end
end