clc
close all
clear all

train_samples = [100, 200, 500, 1000, 2000, 5000];
net_names = ["oneH", "twoH", "fiveH", "oneT", "twoT", "fiveT"];
% Generate Testing Data
Dtest = HW3Q1GenerateData(100000);
% Theoretically Optimal Classifier
% MAP classifier (Given a sample, decide on the class label which leads to max posterior probability)

opt_min_error = MAP(Dtest);
fprintf("MAP gives probability of error for %d sample testing data = %f \n", Dtest.samples, opt_min_error);

method = "patternnet";
K = 10; % 10-fold c.v.
M = 5; % Max five perceptrons
optMLP ={};
minPError = ones(1, length(train_samples));
minPErrorTest = ones(1, length(train_samples));
pErrorVal = ones(K, M);
bestModel = zeros(1, length(train_samples));
for j = 1: length(train_samples)
    
    % Generate Training Data
    N = train_samples(j);
    fprintf("For training samples = %d\n", N);
    Dtrain = HW3Q1GenerateData(N);
    x = Dtrain.x';
    y = Dtrain.y';
    label = Dtrain.label';

    % Determine/specify sizes of parameter matrices/vectors
    nX = size(x,1); % inputs 
    nY = length(unique(label)); % outputs

    % Divide the data set into K approximately-equal-sized partitions
    dummy = ceil(linspace(0,N,K+1));
    for k = 1:K
        indPartitionLimits(k,:) = [dummy(k)+1,dummy(k+1)];
    end

    % Model Order Selection
    % Try several numbers-of-perceptrons   
    for m = 1:M
        alpha = 1/sqrt(m);
        sizeParams = [nX;m;nY];
        % K-fold cross validation
        for k = 1:K
            indValidate = [indPartitionLimits(k,1):indPartitionLimits(k,2)];
            xValidate = x(:, indValidate); % Using folk k as validation set
            yValidate = y(:, indValidate);
            lValidate = label(:, indValidate);
            if k == 1
                indTrain = [indPartitionLimits(k+1,1):N];
            elseif k == K
                indTrain = [1:indPartitionLimits(k-1,2)];
            else
                indTrain = [1:indPartitionLimits(k-1,2),indPartitionLimits(k+1,1):N];
            end
            xTrain = x(:, indTrain); % using all other folds as training set
            lTrain = label(:, indTrain);
            yTrain = y(:, indTrain);
            Ntrain = length(indTrain); Nvalidate = length(indValidate);
            
            if method == "patternnet"
                % Train model parameters
                [pErrorVal(k,m), ~] = nnModel(m, xTrain, lTrain, xValidate, lValidate);
            else
                % Initialize model parameters
                params.A = zeros(m,nX) + alpha.*randn(m,nX);
                params.b = zeros(m,1);
                params.C = zeros(nY,m) + alpha.*randn(nY,m);
                params.d = zeros(nY,1); 
                vecParamsInit = [params.A(:);params.b;params.C(:);params.d];
                % Optimize model
                options = optimset('MaxFunEvals',2e2*length(vecParamsInit),'Display', 'off'); % Matlab default is 200*length(vecParamsInit)
                vecParams = fminsearch(@(vecParams)(objectiveFunction(xTrain,lTrain,sizeParams,vecParams)),vecParamsInit,options);
                params = reshapeParams(sizeParams, vecParams);
                hValidate = mlpModel(xValidate,params);
                % The decision which leads to max probability is chosen(after softmax)
                [~,dValidate] = max(hValidate,[],1); 
                pErrorVal(k,m) = 1-length(find(dValidate==lValidate))/Nvalidate;
            end
        end
    end
    [pErrorBestM,bestM] = min(mean(pErrorVal, 1));
    bestModel(j) = bestM;
    % Train model parameters
    if method == "patternnet"
        [minPError(j), optMLP.(net_names(j) + "samples")] = nnModel(bestM, x, label, x, label);
        net = optMLP.(net_names(j) + "samples");
        h = net(Dtest.x');
    else
        sizeParams(2) = bestM;
        optMLP.(net_names(j) + "samples") = params;
        for i = 1:10
            % Initialize model parameters
            params.A = zeros(bestM,nX) + alpha*randn(bestM,nX);
            params.b = zeros(bestM,1);
            params.C = zeros(nY,bestM) + alpha*randn(nY, bestM);
            params.d = zeros(nY,1); 
            vecParamsInit = [params.A(:);params.b;params.C(:);params.d];
            % Optimize model
            options = optimset('MaxFunEvals',2e2*length(vecParamsInit),'Display', 'off'); % Matlab default is 200*length(vecParamsInit)
            vecParams = fminsearch(@(vecParams)(objectiveFunction(x,label,sizeParams,vecParams)),vecParamsInit,options);
            params = reshapeParams(sizeParams, vecParams);
            h = mlpModel(x,params);
            % The decision which leads to max probability is chosen(after softmax)
            [~,d] = max(h,[],1); 
            if (1-length(find(d==label))/N) < minPError(j)
                minPError(j) = 1-length(find(d==label))/N;
                optMLP.(net_names(j) + "samples") = params;
            end
            fprintf("Iteration number = %d\n", i);
        end
        h = mlpModel(Dtest.x', optMLP.(net_names(j) + "samples"));
    end
    % The decision which leads to max probability is chosen(after softmax)
    [~,d] = max(h,[],1);
    minPErrorTest(j) = 1-length(find(d==Dtest.label'))/size(Dtest.x, 1);
end

plot_graphs(train_samples, minPErrorTest, bestModel, opt_min_error);

function [min_error] = MAP(dataset)
    x = dataset.x;
    decision = zeros(dataset.samples, 1);
    for i = 1: dataset.samples
        p = [0; 0; 0; 0];
        p(1) = mvnpdf(x(i, :), dataset.mean(1, :), dataset.sigma(1:3, :))*dataset.prior(1);
        p(2) = mvnpdf(x(i, :), dataset.mean(2, :), dataset.sigma(4:6, :))*dataset.prior(2);
        p(3) = mvnpdf(x(i, :), dataset.mean(3, :), dataset.sigma(7:9, :))*dataset.prior(3);
        p(4) = mvnpdf(x(i, :), dataset.mean(4, :), dataset.sigma(10:12, :))*dataset.prior(4);
        [~, maxInd] = max(p);
        decision(i) = maxInd;
    end
    min_error = sum(decision~=dataset.label)/dataset.samples;
end

function params = reshapeParams(sizeParams, vecParams)
    nX = sizeParams(1);
    nPerceptrons = sizeParams(2);
    nY = sizeParams(3);
    params.A = reshape(vecParams(1:nX*nPerceptrons),nPerceptrons,nX);
    params.b = vecParams(nX*nPerceptrons+1:(nX+1)*nPerceptrons);
    params.C = reshape(vecParams((nX+1)*nPerceptrons+1:(nX+1+nY)*nPerceptrons),nY,nPerceptrons);
    params.d = vecParams((nX+1+nY)*nPerceptrons+1:(nX+1+nY)*nPerceptrons+nY);
end

function objFncValue = objectiveFunction(X,label,sizeParams,vecParams)
    N = size(X,2); % number of samples
    params = reshapeParams(sizeParams, vecParams);
    H = mlpModel(X,params);
    Y = zeros(size(H));
    for i=1:length(label)
        Y(label(i), i) = 1;
    end
    % Change the objective function appropriately
    % objFncValue = sum(sum((Y-H).*(Y-H),1),2)/N; % MSE for regression under AWGN model
    objFncValue = sum(-sum(Y.*log(H),1),2)/N; % CrossEntropy for ClassPosterior approximation
end

function H = mlpModel(X,params)
    N = size(X,2);                          % number of samples
    nY = length(params.d);                  % number of outputs
    U = params.A*X + repmat(params.b,1,N);  % u = Ax + b, x \in R^nX, b,u \in R^nPerceptrons, A \in R^{nP-by-nX}
    Z = activationFunction(U);              % z \in R^nP, using nP instead of nPerceptons
    V = params.C*Z + repmat(params.d,1,N);  % v = Cz + d, d,v \in R^nY, C \in R^{nY-by-nP}
    % H = activationFunction(V); % linear output layer activations
    H = exp(V)./repmat(sum(exp(V),1),nY,1); % softmax nonlinearity for second/last layer
    % Activate the softmax function to make this MLP a model for class posteriors
end
function out = activationFunction(in)
    % out = in./sqrt(1+in.^2); % ISRU - ramp style nonlinearity
    out = log(1 + exp(in)); % Smooth Relu/Softplus
end

function [minPError, net] = nnModel(nPerceptrons, xTrain, lTrain, xValidate, lValidate)
    net = patternnet(nPerceptrons);
    net.layers{1}.transferFcn = 'poslin';
    out = zeros(4, size(xTrain, 2));
    for ind=1:4
        out(ind,:) = (lTrain==ind);
    end
    net = train(net, xTrain, out);
    h = net(xValidate);
    [~,decision] = max(h,[],1);
    minPError = 1-length(find(decision==lValidate))/size(xValidate, 2);
end

function plot_graphs(train_samples, minPError, bestModel, opt_min_error)
    figure;
    semilogx([50, 10000], [opt_min_error, opt_min_error], '--', train_samples, minPError,'o');
    ylim([0, 0.5]);
    grid on;
    title("Minimum Probability of Error vs number of training samples");
    xlabel("Number of Samples in training data");
    ylabel("Minimum Error Probability (on testing data)");
    legend(["Optimal minimum probability of error", "Minimum Probability of Error achieved on testing data"])
    
    figure;
    semilogx(train_samples,bestModel,'o','MarkerFaceColor','b');
    grid on;
    title("Best Model Dimension vs number of training samples");
    xlabel("Number of Samples in training data");
    ylabel("Optimal number of Perceptrons");
end
    